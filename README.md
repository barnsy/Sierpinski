# Recursive Sierpiński carpet re-written in C from a Java university assignment.  

Size of the board can be changed by updating the below values in the code.  

```
#define size 81
char board[81][81];
```
