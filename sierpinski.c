#include <stdio.h>

// Set size of board.
#define size 81

// Create two dimensional char array for the board.
char board[81][81];

// Remove center function, takes in co-ordinates and removes the centers of the correct size.
void removeCenter(int originx, int originy, int n) {
    for (int a = 0; a < n; a++) {
        for (int b = 0; b < n; b++) {
            board[originx + a][originy + b] = ' ';
        }
    }
}

// Recursive Sierpinski function.
void sierpinski(int x, int y, int n) {
    // Variable to calculate section of the board.
    int width = n / 3;

    // Default parameter for smallest board size.
    if (n == 3) {
        board[(x+1)][(y+1)] = ' ';
    }
    else {
        // Nested for loops to navigate through each part of the board.
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (i == 1 && j == 1) {
                    // Call removeCenter function a current
                    removeCenter(x + width, y + width, width);
                }
                else {
                    // Recursive Sierpinski call to create recursive pattern.
                    sierpinski(x + (i * width), y + (j * width), width);
                }
            }
        }
    }
}


void blankBoard() {
    // Nested for loop to create solid board of '*' at required size.
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            board[i][j] = '*';
        }
    }

    // Call Sierpinski function.
    sierpinski(0,0, size);
}



//int main(String[] args) {
int main(void) {
    // Call blank board function.
    blankBoard();

    // Nested for loop to print out two dimensional char array as string.
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            printf("%c ", board[i][j]);
        }
        printf("\n");
    }
    return 0;
}
